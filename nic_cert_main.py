#!/usr/bin/env python3

import os
import inspect
from bash import bash
from plumbum import local
from common.beaker_cmd import (bash,enter_phase,log,send_command)
from common.beaker_cmd import (i_am_server,i_am_client)
from common.beaker_cmd import (sync_wait,sync_set,run)
from conf_check import ConfUtil

system_version_id = int(os.environ.get("SYSTEM_VERSION_ID"))
client_target = "CLIENT"
server_target = "SERVER"
#################################################################################
#################################################################################
#################################################################################
#################################################################################
def get_env(var_name):
    return os.environ.get(var_name)

def os_check():
    log("Begin OS Check Now")
    import getpass
    if get_env("ID") != 'rhel':
        log("system distro not correct")
        return 1
    if getpass.getuser() != "root":
        log("User check ,must be logged in as root")
        return 1
    return 0

#def conf_checks():
#    proc_cmdline_info = local.path("/proc/cmdline").read()
#    log(proc_cmdline_info)
#    if not "intel_iommu=on" in proc_cmdline_info:
#        log("Iommu Enablement" "Please enable IOMMU mode in your grub config")
#        return 1
#    else:
#        log("Check intel_iommu=on SUCCESS")
#
#    if bash("tuned-adm active | grep cpu-partitioning").value() == '':
#        log("Tuned-adm cpu-partitioning profile must be active")
#        return 1
#    else:
#        log("tuned-adm active OK")
#
#    if bash(""" cat /proc/cmdline  | grep "nohz_full=[0-9]"  """).value() == '':
#        log("Tuned Config Must set cores to isolate in tuned-adm profile")
#        return 1
#    else:
#        log("nohz_full flag check is OK")
#    return 0
#
#def hugepage_checks():
#    log("*** Checking Hugepage Config ***")
#    ret = bash("""cat /proc/meminfo | awk /Hugepagesize/ | awk /1048576/""").value()
#    if ret:
#        log("Hugepage Check OK")
#    else:
#        log("Hugepage Check Failed" "Please enable 1G Hugepages")
#        return 1
#    return 0

def config_file_checks():
    log("*** Checking Config File ***")
    o_conf = ConfUtil()
    try:
        ret_val = o_conf.conf_check()
    except Exception as e:
        print(e)
        return 1
    if ret_val != 0:
        log("Config file invalid , Please help check")
        return 1
    return 0

def rpm_check():
    log("*** Checking for installed RPMS ***")

#    if bash("rpm -qa | grep ^openvswitch").value() == "":
#        log("Openvswitch rpm" "Please install Openvswitch rpm")
#        return 1
#    else:
#        log("Openvswitch rpm check OK")
#
#    if bash("rpm -qa | grep dpdk-[0-9]").value() == "":
#        log("Please install dpdk package rpm ")
#        return 1
#    else:
#        log("dpdk package check OK")
#
#    log("Please make sure qemu-kvm qemu-kvm-tools version >= 2.12 !!!!")
#    log("Please make sure qemu-kvm qemu-kvm-tools version >= 2.12 !!!!")
#    log("Please make sure qemu-kvm qemu-kvm-tools version >= 2.12 !!!!")
#    if system_version_id < 80:
#        if bash("rpm -qa | grep qemu-kvm-tools").value() == "":
#            log("Please install qemu-kvm-tools rpm ")
#            return 1
#        else:
#            log("qemu-kvm-tools check OK")
#    else:
#        if bash("rpm -qa | grep kernel-tools").value() == "":
#            log("Please install kernel-tools rpm ")
#            return 1
#        else:
#            log("kernel-tools check OK")

    if bash("rpm -qa | grep qemu-img").value() == "":
        log("Please install qemu-img rpm ")
        return 1
    else:
        log("qemu-img package check OK")

    if bash("rpm -qa | grep qemu-kvm").value() == "":
        log("Please install qemu-kmv rpm ")
        return 1
    else:
        log("qemu-kvm package check OK")
    return 0

#def network_connection_check():
#    log("*** Checking connection to people.redhat.com ***")
#    ret = bash("ping -c 10 people.redhat.com")
#    log(ret)
#    if ret.code == 0:
#        log("*** Connection to server succesful ***")
#    else:
#        log("People.redhat.com connection fail !!!!")  
#        log("Cannot connect to people.redhat.com, please verify internet connection !!!")
#        return 1
#    return 0

#def ovs_running_check():
#    log("*** Checking for running instance of Openvswitch ***")
#    if bash("pgrep ovs-vswitchd || pgrep ovsdb-server").value():
#        log("It appears Openvswitch may be running, please stop all services and processes")
#    else:
#        log("ovs-vswitchd and ovsdb-server check OK")
#    return 0

def exit_with_error(str):
    print(f"Exit with {str}")
    log(f"""Exit with {str}""")
    send_command("rhel-nic-cert-gitlab-quit-string")
    pass

def get_test_nic():
    func_name = inspect.stack()[0][3]
    sync_start = func_name + "_start"
    sync_end = func_name + "_end"
    if i_am_server():
        server_nic = get_env("SERVER_NIC1")
        cmd = f"""
        ip li set {server_nic} up
        ip addr flush {server_nic}
        ip addr add 192.168.99.100/24 dev {server_nic}
        """
        run(cmd)
        sync_set(client_target,sync_start)
        sync_wait(client_target,sync_end)
        os.environ["NIC_TEST"] = server_nic
        log(f"CURRENT test nic name is {server_nic}")
    elif i_am_client():
        client_nic1 = get_env("CLIENT_NIC1")
        client_nic2 = get_env("CLIENT_NIC2")
        cmd = f"""
        ip li set {client_nic1} up
        ip li set {client_nic2} up
        ip addr flush {client_nic1}
        ip addr flush {client_nic2}
        """
        run(cmd)
        sync_wait(server_target,sync_start)
        test_cmd = f"""
        ip addr add 192.168.99.99/24 dev {client_nic1}
        ping 192.168.99.100 -I {client_nic1} -c 10
        """
        if bash(test_cmd).code == 0:
            os.environ["NIC_TEST"] = client_nic1
        else:
            os.environ["NIC_TEST"] = client_nic2
        cmd = f"""
        ip addr flush {client_nic1}
        ip addr flush {client_nic2}
        """
        run(cmd)
        client_nic = os.environ["NIC_TEST"]
        log(f"CURRENT test nic name is {client_nic}")
        sync_set(server_target,sync_end)
    else:
        return None

def main():
    with enter_phase("OS DISTRO CHECK"):
        ret = os_check()
        if ret != 0:
            exit_with_error("OS CHECK FAILED")
        pass
#    with enter_phase("HUGEPAGE CHECK"):
#        ret = hugepage_checks()
#        if ret != 0:
#            exit_with_error("HUGEPAGE INVALID")
#        pass
#    with enter_phase("CONFIG CHECK"):
#        ret = conf_checks()
#        if ret != 0:
#            exit_with_error("/proc/cmdline check FAILED")
#        pass
    with enter_phase("CONFIG FILE CHECK"):
        ret = config_file_checks()
        if ret != 0:
            exit_with_error("CONFIG FILE INVALID")
        pass
    with enter_phase("RPM PACKAGES CHECK"):
        ret = rpm_check()
        if ret != 0:
            exit_with_error("RPM CHECK FAILED")
        pass
#    with enter_phase("NETWORK CONNECTION CHECK"):
#        ret = network_connection_check()
#        if ret != 0:
#            exit_with_error("NETWORK CONNECTION CHECK FAILED")
#        pass
    with enter_phase("GET TEST NIC"):
        get_test_nic()
        pass
    task_list = ConfUtil().get_default_section_config()["TASKS_LIST"]
    for task in task_list.split(','):
        with enter_phase(f"TASK {task} Testing"):
            if str(task) == "SRIOV":
                from sriov_test.sriov_main import main as sr_main
                sr_main()
                pass
            elif str(task) == "OVS-DPDK":
                pass
            elif str(task) == "OVS-KERNEL":
                pass
            elif str(task) == "OVS-KERNEL":
                pass
            elif str(task) == "DPDK-STANDALONE":
                pass
            elif str(task) == "HARDWARE-OFFLOAD":
                pass
            else:
                log(f"Task {task} Unsupported now")
                pass

if __name__ == "__main__":
    send_command("rlJournalStart")
    main()
    send_command("rlJournalPrintText")
    send_command("rlJournalEnd")
    send_command("rhel-nic-cert-gitlab-quit-string")
    pass
