#!/usr/bin/env python3

import configparser
import fire
import os
import requests
import functools
print = functools.partial(print, flush=True)

def check_nic_exists(nic_name):
    return os.path.exists(f"/sys/class/net/{nic_name}")

def check_vcpu_exists(cpu_id):
    return os.path.exists(f"/sys/class/cpuid/cpu{cpu_id}")

#pmd_mask is hex value
def get_cpu_list_from_mask(pmd_mask):
    cpu_list = []
    try:
        if not str(pmd_mask).startswith("0x"):
            temp_pmd_mask = str("0x") + str(pmd_mask)
        else:
            temp_pmd_mask = str(pmd_mask)
        # print(temp_pmd_mask)
        bin_value = bin(int(temp_pmd_mask,16))[2:][::-1]
        index = 0
        while index < len(bin_value):
            if bin_value[index] == '1':
                cpu_list.append(index)
            index += 1
    except Exception as e:
        print(e)
    finally:
        pass
    return cpu_list

def check_url_exists(url):
    ret = requests.head(url)
    return ret.status_code == 200

class ConfUtil(object):
    def __init__(self,conf_file="nic_cert.conf"):
        dir_path = os.path.dirname(os.path.realpath(__file__))
        with open(dir_path + "/" + conf_file, 'r') as f:
            config_string = '[DEFAULT]\n' + f.read()
        self.config = configparser.ConfigParser()
        try:
            self.config.read_string(config_string)
        except Exception as e:
            self.config = None
            print(e)
        finally:
            pass
        pass

    def get_config(self):
        return self.config

    def get_default_section_config(self):
        return self.config["DEFAULT"]

    def conf_check(self):
        print("Start Config File Check")
        self.pmd_cpu_list = []
        task_list = [
            "SRIOV",
            "OVS-DPDK",
            "OVS-KERNEL",
            "DPDK-STANDALONE",
            "HARDWARE-OFFLOAD"
        ]
        default_sec = self.config["DEFAULT"]
        key_list_sriov = [
        "SINGLE_PORT_MODE",
        "SRIOV_TXD_SIZE",
        "SRIOV_RXD_SIZE"
        ]
        key_list_without_sriov = [
        "SERVERS",
        "CLIENTS",
        "SERVER_NIC1",
        "SERVER_NIC2",
        "CLIENT_NIC1",
        "CLIENT_NIC2",
#        "TXD_SIZE",
#        "RXD_SIZE",
        "ONE_QUEUE_IMAGE",
#        "TWO_QUEUE_IMAGE",
#        "DPDK_VER",
#        "DPDK_URL",
#        "DPDK_TOOL_URL",
#        "TREX_URL"
        ]
        #get servers and clients hostname
        server_host = default_sec["SERVERS"]
        client_host = default_sec["CLIENTS"]
        current_host = os.environ["HOSTNAME"]
        i_am_server_flag = False
        i_am_client_flag = False
        if server_host == current_host:
            i_am_server_flag = True
        elif client_host == current_host:
            i_am_client_flag = True
        else:
            raise Exception(f"The Host {current_host} neither SERVERS nor CLIENTS")

        #Get Taskslist First
        if "TASKS_LIST" not in default_sec.keys():
            print(f"Config item TASKS_LIST does not exists, Please check it")
            return 1
        #check task list format and content
#        for task in default_sec["TASKS_LIST"].split(','):
#            if task not in task_list:
#                print(f"Invalid task {task}, Please check it")
#                return 1
#        if "SRIOV" in default_sec["TASKS_LIST"]:
#            for k in key_list_sriov:
#                if k not in default_sec.keys():
#                    print(f"Config item {k} does not exists, Please check it")
#                    return 1
#                else:
#                    val = default_sec[k]
#                    if k == "SRIOV_TXD_SIZE" or k == "SRIOV_RXD_SIZE":
#                        assert int(val) > 0 ,f"{k} val should > 0"
#                    #only check server side
#                    if i_am_server_flag:
#                        if k == "SINGLE_PORT_MODE":
#                            assert str(val).lower() == "true" or str(val).lower() == "false",f"{k} value invalid, It should true or false"
#                        else:
#                            pass
#                print(f"Current key {k} check : PASS")
        for k in key_list_without_sriov:
            if k not in default_sec.keys():
                print(f"Config item {k} does not exists, Please check it")
                return 1
            else:
                val = default_sec[k]
                if k == "SERVERS" or k == "CLIENTS":
                    assert len(str(val)) > 0 ,f"{k} does not exists , Please help check"
                if i_am_server_flag:
                    if k == "SERVERS":
                        assert str(val) == os.environ["HOSTNAME"] == server_host,f"{k} set invalid , Please help check"
                if i_am_client_flag:
                    if k == "CLIENTS":
                        assert str(val) == os.environ["HOSTNAME"] == client_host,f"{k} set invalid , Please help check"

                if k == "SERVER_NIC1" or k == "SERVER_NIC2" or k == "CLIENT_NIC1" or k == "CLIENT_NIC2":
                    assert len(str(val)) > 0 ,f"{k} does not exists , Please help check"
                if i_am_server_flag:
                    if k == "SERVER_NIC1" or k == "SERVER_NIC2":
                        assert check_nic_exists(val) == True,f"NIC {val} does not exists , Please help check"
                if i_am_client_flag:
                    if k == "CLIENT_NIC1" or k == "CLIENT_NIC2":
                        assert check_nic_exists(val) == True,f"NIC {val} does not exists , Please help check"

                if k == "TXD_SIZE" or k == "RXD_SIZE":
                    assert int(val) > 0 ,f"{k} val {val} should > 0"

#                if k == "ONE_QUEUE_IMAGE" or k == "TWO_QUEUE_IMAGE":
                if k == "ONE_QUEUE_IMAGE":
                    assert check_url_exists(val) == True,f"{k} Link check failed , Please help check"

#                if k == "DPDK_VER":
#                    import re
#                    pattern = re.compile("^[1-2][0-9][0-9][0-9]-[0-9]+$")
#                    ret_val = re.fullmatch(pattern,str(val))
#                    assert ret_val != None,f"{k} value {val} format invalid, Please input like 1811-2 "
#                    pass
#
#                if k == "DPDK_URL" or k == "DPDK_TOOL_URL" or k == "TREX_URL":
#                    assert check_url_exists(val) == True,f"{k} Link check failed , Please help check"
#            print(f"Current key {k} check : PASS")
        print("End Config File Check")
        return 0

if __name__ == '__main__':
    fire.Fire(ConfUtil)