#!/usr/bin/env bash
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Description: Run RHEL NIC CERTIFICATION tests
#   Author: Christian Trautman <ctrautma@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2017 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

TEST="NIC Certification PFT Test"

# Task list
TASKS_LIST=SRIOV,OVS-DPDK,OVS-KERNEL,DPDK-STANDALONE,HARDWARE-OFFLOAD

#servers and clients
SERVERS=DUT1.example.com
CLIENTS=DUT2.example.com

# NIC Device names such as p6p1 p6p2
# SERVER_NIC1 : server host nic1 name 
# SERVER_NIC2 : server host nic2 name
SERVER_NIC1=enp132s0f0
SERVER_NIC2=enp132s0f1
# CLIENT_NIC1 : client host nic1 name
# CLIENT_NIC2 : cleint host nic2 name
CLIENT_NIC1=enp4s0f0
CLIENT_NIC2=enp4s0f1

# TESTPMD descriptor size
# It can be used to modify descriptor sizes inside of VM when running TESTPMD 
# It is used by both dpdk and kernel and throughput test
# SR-IOV options can be used to modify sr-iov descriptor sizes
# Normally , Just keep the default value is OK
TXD_SIZE=512
RXD_SIZE=512

#IMAGE INFO FOR TESTING
ONE_QUEUE_IMAGE=#to be updated
TWO_QUEUE_IMAGE=#to be updated

# As to dpdk-18.11.2-1.el7.x86_64.rpm ,version is 1811-2 ,PLease MUST follow this format.
# For DPDK_URL AND DPDK_TOOL_URL Only rpm format was supported
DPDK_VER=2111-2
DPDK_URL=#The URL to download DPDK rpm
DPDK_TOOL_URL=#The URL to download DPDK tool rpm

# Trex url will be used for testing
# Example : https://trex-tgn.cisco.com/trex/release/v2.87.tar.gz
TREX_URL=#https://trex-tgn.cisco.com/trex/release/v2.87.tar.gz

# SR-IOV only Config Information
# single port mode
# This mode specify out which port or ports will be used
# There are two valid value for this option, true,false
# if true was set, all test cases will use only first port (NIC1,NIC1_VF)of all below config
# if false was set , all test case will use all ports of all below config
SINGLE_PORT_MODE=false

# SR-IOV only Config Information
SRIOV_TXD_SIZE=2048
SRIOV_RXD_SIZE=2048
